# ex-mariadb

docker コンテナで動作するアプリケーションは、コンテナ内に自由にファイルをつくることができます。 しかし、コンテナが終了すると変更は失われてしまいます。 nginx の例のように特定のディレクトリを外部にマッピングしてもよいですが、 docker ボリュームにしておくと扱いがらくになります。

このリポジトリ <https://gitlab.com/nekonaq.ex/ex-mariadb.git> を clone して docker-compose up してください。 これは mariadb ( mysql のフォーク ) のコンテナです。

docker-compose.yml は少し複雑ですが、必要なところだけ順に説明していきます。

コンテナを起動したら、WSL2 にクライアントをインストールしてあれば mysql コマンドで db に接続できます。

    apt-get install -y mariadb-client

    $ mysql -h 127.0.0.1 -u root -p
    Enter password: ****
    
    mysql> show databases;
    show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | sys                |
    +--------------------+
    4 rows in set (0.00 sec)
    
    mysql> CREATE DATABASE webapp CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
    Query OK, 1 row affected (0.00 sec)
    
    mysql> Bye

最初に起動したとき、データベース・ファイルがありませんので自動的に作成してくれます。 データベース・ファイルの root アカウントの初期パスワードは、ここのディレクトリにある `secrets/mysql-root-password` に書いてあります。

docker-compose.yml に書いてある environment のMYSQL\_ROOT\_PASSWORD\_FILE と、 volumes の /run/secrets は、このために書いています。

volumes の /etc/mysql/conf.d は、mariadb サーバーの設定を事情により一部変更したいので書いています。

volumes の /var/lib/mysql が、データベース・ファイルを mysql という docker ボリュームに割り当てるためのものです。

docker ボリュームの一覧は docker volume ls コマンドで確認できます。

    $ docker volume ls
    DRIVER    VOLUME NAME
    local     459a12b567fc980fb9fc35d75bcf93be878511426785da84598c3a02ff53fde1
    local     ex-mariadb_mysql
    local     redis_redis

ex-mariadb\_mysql というのが、いま docker-compose up で作成されたボリュームです。 これを docker volume rm で削除すれば、データベース・ファイルがない状態からやりなおしできます。

docker-compose up を Ctrl-C で終了して、こんどはバックグラウンドで起動し、 さきほどつくったデータベース webapp が残っていることを確認します。

    $ docker-compose up -d
    
    $ mysql -h 127.0.0.1 -u root -p webapp
    Enter password: ****
    
    mysql> show tables;
    Empty set (0.01 sec)
    
    mysql> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | sys                |
    | webapp             |
    +--------------------+
    5 rows in set (0.00 sec)
    
    mysql> Bye
    
    $ docker-compose down

WSL2 でアプリケーションを開発するとして、MariaDB (MySQL) が必要ならこれで充分だと思います。

ここの docker-compose.yml でやっているようないろいろな設定変更のやり方や、 用意されているイメージのバージョンなどの情報は dockerhub なら各ベンダーの公式ページに書いてあります。 MariaDB の場合は: <https://hub.docker.com/_/mariadb>

MariaDB 以外にも、たとえば elasticsearch + kibana も全部 docker で起動できます。